/*
 * Copyright VLIPCODE - 2017
 * Todos los derechos reservados
 */
package mod.comunicacion.util;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <b>
 * Descripción de clase.
 * </b>
 *
 * @author christian
 * <p>
 * [$Author: christian $, $Date: 2016-12-31
 * </p>
 */
public class Log {

    public static void log(Class clase, Exception exception, String mensaje) {
        Logger.getLogger(Log.class.getName()).log(Level.SEVERE, mensaje, exception);
    }

    public static void log(Class clase, String mensaje) {
        Logger.getLogger(Log.class.getName()).log(Level.INFO, mensaje);
    }

}
