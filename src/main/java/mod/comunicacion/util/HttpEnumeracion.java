/*
 * Copyright VLIPCODE - 2017
 * Todos los derechos reservados
 */
package mod.comunicacion.util;

import lombok.Getter;
import lombok.Setter;

/**
 * <b>
 * Descripción de clase.
 * </b>
 *
 * @author christian
 * <p>
 * [$Author: christian $, $Date: 2016-12-31
 * </p>
 */
public enum HttpEnumeracion {

    CONTENIDO("application/json"), TIPO_CONTENIDO("Content-Type"), TIEMPO_ESPERA("400000"), CODIGO_EXITO_PETICION("200"), ERROR_LLAMADA_SERVICIO("Hubo un error al llamar al servicio web."), UTF8("UTF-8");

    @Getter
    @Setter
    private String nemonico;

    private HttpEnumeracion(String nemonico) {
        this.nemonico = nemonico;
    }
}
