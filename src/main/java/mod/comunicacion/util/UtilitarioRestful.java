/*
 * Copyright VLIPCODE - 2017
 * Todos los derechos reservados
 */
package mod.comunicacion.util;

import com.google.gson.Gson;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import mod.comunicacion.to.RespuestaTo;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.GzipDecompressingEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * <b>
 * Descripción de clase.
 * </b>
 *
 * @author christian
 * <p>
 * [$Author: christian $, $Date: 2016-12-31
 * </p>
 */
public class UtilitarioRestful {

    public static <T> T llamarServicioWeb(String urlServicioWeb, Object objetoModelo, Class claseRetorno, String token) {
        RequestConfig configuracion = ConfiguracionHttp.obtenerConfiguracionZip();
        try (CloseableHttpClient cliente = HttpClients.custom().setDefaultRequestConfig(configuracion).build()) {
            Gson gson = new Gson();
            HttpPost envio = new HttpPost(urlServicioWeb);
            String json = gson.toJson(objetoModelo);
            StringEntity params = new StringEntity(json, HttpEnumeracion.UTF8.getNemonico());
            envio.addHeader(HttpEnumeracion.TIPO_CONTENIDO.getNemonico(), HttpEnumeracion.CONTENIDO.getNemonico());
            envio.setEntity(params);
            HttpResponse resultado = cliente.execute(envio);
            if (resultado.getStatusLine().getStatusCode() != Integer.parseInt(HttpEnumeracion.CODIGO_EXITO_PETICION.getNemonico())) {
                throw new RuntimeException(HttpEnumeracion.ERROR_LLAMADA_SERVICIO.getNemonico());
            }
            String jsonRespuesta = EntityUtils.toString(resultado.getEntity(), StandardCharsets.UTF_8);
            return (T) gson.fromJson(jsonRespuesta, claseRetorno);
        } catch (IOException e) {
            Log.log(UtilitarioRestful.class, e, "ERROR");
            return null;
        }

    }

    public static String devuelveJsonObjeto(Object modelo) {
        Gson gson = new Gson();
        return gson.toJson(modelo);
    }

    public static <T> T devuelveObjetoJson(String json, Class claseRetorno) {
        Gson gson = new Gson();
        return (T) gson.fromJson(json, claseRetorno);
    }
    
    public static void validarRespuestaServicioWeb(RespuestaTo respuesta) throws SistemaExcepcion {
        if (!respuesta.getCodigo().equals(CodigoWebServiceEnum.INFO.getCodigo())) {
            throw new SistemaExcepcion(respuesta.getMensaje());
        }
    }
}
