/*
 * Copyright VLIPCODE - 2017
 * Todos los derechos reservados
 */
package mod.comunicacion.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.http.client.config.RequestConfig;

/**
 * <b>
 * Descripción de clase.
 * </b>
 *
 * @author christian
 * <p>
 * [$Author: christian $, $Date: 2016-12-31
 * </p>
 */
public class ConfiguracionHttp {

    public static final int TIEMPO_ESPERA = 3000;

    public static RequestConfig obtenerConfiguracionZip() {
        RequestConfig defaultRequestConfig = RequestConfig.custom()
                .setSocketTimeout(Integer.parseInt(HttpEnumeracion.TIEMPO_ESPERA.getNemonico()))
                .setConnectTimeout(Integer.parseInt(HttpEnumeracion.TIEMPO_ESPERA.getNemonico()))
                .setConnectionRequestTimeout(Integer.parseInt(HttpEnumeracion.TIEMPO_ESPERA.getNemonico()))
                .setContentCompressionEnabled(false).build();
        return defaultRequestConfig;
    }

    public static final boolean pingJson(String url, int timeout) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);
            connection.setRequestMethod("HEAD");
            connection.setInstanceFollowRedirects(true);
            HttpURLConnection.setFollowRedirects(true);
            int responseCode = connection.getResponseCode();
            if (HttpURLConnection.HTTP_MOVED_PERM == responseCode || HttpURLConnection.HTTP_MOVED_TEMP == responseCode || HttpURLConnection.HTTP_SEE_OTHER == responseCode) {
                String newUrl = connection.getHeaderField("Location");
                connection = redireccionar(newUrl, timeout);
                responseCode = connection.getResponseCode();
            }
            return HttpURLConnection.HTTP_BAD_METHOD == responseCode || HttpURLConnection.HTTP_UNAUTHORIZED == responseCode;
        } catch (RuntimeException | IOException exception) {
            Log.log(ConfiguracionHttp.class, exception, "Error al validar el servicio rest");
            return false;
        }
    }

    private static HttpURLConnection redireccionar(String url, int timeout) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setConnectTimeout(timeout);
        connection.setReadTimeout(timeout);
        connection.setRequestMethod("HEAD");
        connection.setInstanceFollowRedirects(true);
        HttpURLConnection.setFollowRedirects(true);
        int responseCode = connection.getResponseCode();
        if (HttpURLConnection.HTTP_MOVED_PERM == responseCode || HttpURLConnection.HTTP_MOVED_TEMP == responseCode || HttpURLConnection.HTTP_SEE_OTHER == responseCode) {
            String newUrl = connection.getHeaderField("Location");
            return redireccionar(newUrl, timeout);
        }
        return connection;
    }
}
