/*
 * Copyright VLIPCODE - 2017
 * Todos los derechos reservados
 */
package mod.comunicacion.util;

import javax.ejb.ApplicationException;

/**
 * <b>
 * Descripción de clase.
 * </b>
 *
 * @author christian
 * <p>
 * [$Author: christian $, $Date: 2016-12-31
 * </p>
 */
@ApplicationException(rollback = true)
public class SistemaExcepcion extends Exception {

    private static final long serialVersionUID = -6072899549002718669L;

    public SistemaExcepcion() {
        super();
    }

    public SistemaExcepcion(String mensaje) {
        super(mensaje);
    }

    public SistemaExcepcion(Throwable t) {
        super(t);
    }

    public SistemaExcepcion(String mensaje, Throwable t) {
        super(mensaje, t);
    }
}
