/*
 * Copyright VLIPCODE - 2017
 * Todos los derechos reservados
 */
package mod.comunicacion.util;

import lombok.Getter;
import lombok.Setter;

/**
 * <b>
 * Descripción de clase.
 * </b>
 *
 * @author christian
 * <p>
 * [$Author: christian $, $Date: 2016-12-31
 * </p>
 */
public enum CodigoWebServiceEnum {
    INFO("1", "OK"), PERMISO_DENEGADO("2", "Permiso Denegado."), ERROR("3", "Error del Sistema, contáctese con el administrador."),
    SERVICIO_NO_DISPONIBLE("4", "Servicio no disponible"), CREDENCIALES_INCORRECTAS("5", "Credenciales Incorrectas."), NO_PRIVILEGIOS("5", "Este usuario no tiene privilegios."), ERROR_REGISTRO_CIVIL("6", "Persona no existe."),
    ERROR_CONTROLADO("9", "Error.");

    @Getter
    @Setter
    private String codigo;
    @Getter
    @Setter
    private String mensaje;

    private CodigoWebServiceEnum(String codigo, String mensaje) {
        this.codigo = codigo;
        this.mensaje = mensaje;
    }

}
