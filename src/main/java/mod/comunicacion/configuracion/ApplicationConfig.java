/**
  * Copyright 2020 Empresa Pública Petroecuador.
  * Todos los derechos reservados.
  */
package mod.comunicacion.configuracion;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
  *
  * @author icaceres
  *
  * @version 1.0
  *
  * 03/02/2020
  *
  * La clase ApplicationConfig.java ha sido creada con el fin de gestionar
  */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(mod.comunicacion.ws.ClasificacionGeneracionWs.class);
    }

}
