/**
 * Copyright 2020 Empresa Pública Petroecuador.
 * Todos los derechos reservados.
 */
package mod.comunicacion.servicio;

import javax.ejb.Stateless;
import javax.inject.Inject;
import mod.comunicacion.to.ClasificacionGeneracionTo;
import mod.comunicacion.util.SistemaExcepcion;

/**
 *
 * @author icaceres
 *
 * @version 1.0
 *
 * 03/02/2020
 *
 * La clase ClasificacionGeneracionServicio.java ha sido creada con el fin de
 * gestionar
 */
@Stateless
public class ClasificacionGeneracionServicio {
    
    @Inject
    private LlamadaServicioWeb llamadaServicioWeb;

    public ClasificacionGeneracionTo clasificarGenerar(ClasificacionGeneracionTo entrada) throws SistemaExcepcion{
        ClasificacionGeneracionTo respuesta = llamadaServicioWeb.obtenerResultadoClasificacionGeneracion(entrada);
        return respuesta;
    }

}
