/**
 * Copyright 2019 Empresa Pública Petroecuador.
 * Todos los derechos reservados.
 */
package mod.comunicacion.servicio;

import javax.ejb.Stateless;
import mod.comunicacion.to.ClasificacionGeneracionTo;
import mod.comunicacion.util.UtilitarioRestful;

/**
 *
 * @author icaceres
 *
 * @version 1.0
 *
 * 26/12/2019
 *
 * La clase LlamadaServicioWeb.java ha sido creada con el fin de gestionar
 */
@Stateless
public class LlamadaServicioWeb {

    private static final String URL_WS_CLASIFICACION = "http://127.0.0.1:8383/clasificar_generar";

    public ClasificacionGeneracionTo obtenerResultadoClasificacionGeneracion(ClasificacionGeneracionTo entrada) {
        String url = URL_WS_CLASIFICACION;
        return UtilitarioRestful.llamarServicioWeb(url, entrada, ClasificacionGeneracionTo.class, null);
    }

}
