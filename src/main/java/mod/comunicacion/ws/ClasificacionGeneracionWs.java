/**
 * Copyright 2020 Empresa Pública Petroecuador.
 * Todos los derechos reservados.
 */
package mod.comunicacion.ws;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import mod.comunicacion.servicio.ClasificacionGeneracionServicio;
import mod.comunicacion.to.ClasificacionGeneracionTo;
import mod.comunicacion.to.RespuestaTo;
import mod.comunicacion.util.SistemaExcepcion;

/**
 *
 * @author icaceres
 *
 * @version 1.0
 *
 * 03/02/2020
 *
 * La clase ClasificacionGeneracionWs.java ha sido creada con el fin de
 * gestionar
 */
@Singleton
@Consumes(MediaType.APPLICATION_JSON)
@Path("clasificacionGeneracion")
@Lock(LockType.READ)
public class ClasificacionGeneracionWs {

    @Inject
    private ClasificacionGeneracionServicio clasificacionGeneracionServicio;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("obtenerResultado")
    public ClasificacionGeneracionTo obtenerResultado(ClasificacionGeneracionTo entrada) {
        ClasificacionGeneracionTo respuesta;
        try {
            respuesta = clasificacionGeneracionServicio.clasificarGenerar(entrada);
            respuesta.setRespuesta(new RespuestaTo("1", "OK"));

            return respuesta;
        } catch (SistemaExcepcion ex) {
            respuesta = new ClasificacionGeneracionTo();
            Logger.getLogger(ClasificacionGeneracionWs.class.getName()).log(Level.SEVERE, null, ex);
            respuesta.setRespuesta(new RespuestaTo("2", "ERROR:" + ex.getLocalizedMessage()));
        }
        return respuesta;
    }

}
