/**
 * Copyright 2020 Empresa Pública Petroecuador.
 * Todos los derechos reservados.
 */
package mod.comunicacion.to;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author icaceres
 *
 * @version 1.0
 *
 * 04/02/2020
 *
 * La clase FormaTo.java ha sido creada con el fin de gestionar
 */
public class FormaTo implements Serializable {

    private static final long serialVersionUID = 9184997850427951547L;

    @Getter
    @Setter
    private String resultado;
    @Getter
    @Setter
    private String diamante;
    @Getter
    @Setter
    private String ovalado;
    @Getter
    @Setter
    private String alargado;
    @Getter
    @Setter
    private String cuadrado;
    @Getter
    @Setter
    private String redondo;
    @Getter
    @Setter
    private String triangulo;
    @Getter
    @Setter
    private String resultadoImg;

    public FormaTo() {
    }

}
