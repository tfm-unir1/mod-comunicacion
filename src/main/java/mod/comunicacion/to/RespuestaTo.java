/**
  * Copyright 2019 Empresa Pública Petroecuador.
  * Todos los derechos reservados.
  */
package mod.comunicacion.to;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
  *
  * @author icaceres
  *
  * @version 1.0
  *
  * 29/10/2019
  *
  * La clase RespuestaTo.java ha sido creada con el fin de gestionar
  */
public class RespuestaTo implements Serializable{

    private static final long serialVersionUID = 1410016624826646678L;
    
    @Getter
    @Setter
    private String codigo;
    @Getter
    @Setter
    private String mensaje;

    public RespuestaTo() {
    }

    public RespuestaTo(String codigo, String mensaje) {
        this.codigo = codigo;
        this.mensaje = mensaje;
    }

}
