/**
  * Copyright 2020 Empresa Pública Petroecuador.
  * Todos los derechos reservados.
  */
package mod.comunicacion.to;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
  *
  * @author icaceres
  *
  * @version 1.0
  *
  * 03/02/2020
  *
  * La clase ClasificacionGeneracionTo.java ha sido creada con el fin de gestionar
  */
public class ClasificacionGeneracionTo implements Serializable {

    private static final long serialVersionUID = 5223668277810433133L;

    @Getter
    @Setter
    private RespuestaTo respuesta;
    @Getter
    @Setter
    private String imagenEntrada;
    @Getter
    @Setter
    private FormaTo forma;

    public ClasificacionGeneracionTo() {
    }
}
